#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK

import argparse
import argcomplete
import subprocess
import configparser
import logging
import os

from yaml import parse

SCRIPTDIR = os.path.dirname(os.path.realpath(__file__))
CONFIG_NAME = "remdev.ini"
CONFIG_FILEPATH = os.path.join(SCRIPTDIR, CONFIG_NAME)

config = configparser.ConfigParser()
config.read(CONFIG_FILEPATH)

assert "REMOTE" in config
assert "USER" in config
assert "addr" in config["REMOTE"]
assert "username" in config["USER"]
assert "homedir" in config["USER"]

REMOTE_USER = config["USER"]["username"]
BS_ADDR = f"{REMOTE_USER}@{config['REMOTE']['addr']}"


def remote_connect(args):
    if "require-vpn" in config["REMOTE"] and config["REMOTE"]["require-vpn"] == "true":
        remote_vpn_connect(args)
    print(f"Connecting to the remote server")
    callProcess = subprocess.call(f"ssh -R2000:localhost:22 {BS_ADDR}", shell=True)


def remote_pull(args):
    print("Downloading file from the remote server")
    output = args.output if args.output is not None else "./"
    cmd = ["scp", "-r", f"{BS_ADDR}:{args.source}", output]
    process = subprocess.call(cmd)


def remote_push(args):
    print("Sending file(s) to the remote server")
    cmd = ["scp", "-r", args.source, f"{BS_ADDR}:{args.dest}"]
    process = subprocess.call(cmd)


def remote_code(args):
    dir = args.dir.replace("~", config["USER"]["homedir"])
    cmd = ["code", "--folder-uri", f"vscode-remote://ssh-remote+{BS_ADDR}{dir}"]
    p = subprocess.Popen(cmd, start_new_session=True)


def remote_vpn_connect(args):
    check_cmd = ["nmcli", "con", "show", "--active", "id", config["VPN"]["name"]]
    process = subprocess.Popen(check_cmd, stdout=subprocess.PIPE)
    streamdata = process.communicate()[0]
    if streamdata:
        logging.warning("VPN already connected")
        return

    cmd = ["nmcli", "con", "up", "id", config["VPN"]["name"]]
    subprocess.call(cmd)

    if "ROUTE" not in config:
        return
    # set route
    check_cmd = ["ip", "r"]
    process = subprocess.Popen(check_cmd, stdout=subprocess.PIPE)
    streamdata = process.communicate()[0]
    if config["ROUTE"]["iface"] in streamdata.decode("utf-8").split("\n", 1)[0]:
        logging.warning("Route already set")
        return
    print("Setting default route...")
    gw_ip = args.ip if args.ip else config["ROUTE"]["gw"]
    iface = args.iface if args.iface else config["ROUTE"]["iface"]
    cmd = ["sudo", "route", "add", "default", "gw", gw_ip, iface]
    logging.debug(f"route cmd: {cmd}")
    process = subprocess.call(cmd)

# MAIN

parser = argparse.ArgumentParser()
parser.add_argument("--ip", dest="ip", help="gateway IP addr")
parser.add_argument("--iface", dest="iface", help="Default connection interface")
parser.add_argument(
    "-d",
    "--debug",
    help="Print lots of debugging statements",
    action="store_const",
    dest="loglevel",
    const=logging.DEBUG,
    default=logging.WARNING,
)
parser.add_argument(
    "-v",
    "--verbose",
    help="Be verbose",
    action="store_const",
    dest="loglevel",
    const=logging.INFO,
)
subparsers = parser.add_subparsers(help="actions")

connect_parser = subparsers.add_parser("connect", help="Connect to the remote server")
connect_parser.set_defaults(func=remote_connect)

pull_parser = subparsers.add_parser("pull", help="Download file from the remote server")
pull_parser.add_argument(
    "source", metavar="remote_path", type=str, help="file to download"
)
pull_parser.add_argument("-o", dest="output", help="Output path")
pull_parser.set_defaults(func=remote_pull)

push_parser = subparsers.add_parser("push", help="Send files to remote server")
push_parser.add_argument("source", metavar="local_path", type=str, help="file to send")
push_parser.add_argument(
    "dest", metavar="remote_path", type=str, help="Transfer destination"
)
push_parser.set_defaults(func=remote_push)

code_parser = subparsers.add_parser("code", help="Open remote folder in VSCode")
code_parser.add_argument("dir", metavar="remote_path", type=str, help="folder to open")
code_parser.set_defaults(func=remote_code)

vpn_parser = subparsers.add_parser("vpn", help="Manage VPN connection")
vpn_subparsers = vpn_parser.add_subparsers(help="actions")
vpn_activate_parser = vpn_subparsers.add_parser(
    "activate", help="Activate VPN connection"
)
vpn_activate_parser.set_defaults(func=remote_vpn_connect)

argcomplete.autocomplete(parser)

args = parser.parse_args()
logging.basicConfig(level=args.loglevel)
try:
    args.func(args)
except AttributeError:
    parser.error("Too few arguments")
