# REMDEV 

Create remdev.init config file in the same directory where remdev script is
located.

Example content of remdev.ini file:
```
[USER]
username = molj
homedir = /home/leckijakub

[REMOTE]
addr = 192.168.0.15
require-vpn = true

[VPN]
name = vpn

[ROUTE]
iface = enx00e04c68327d
gw = 192.168.1.1
```
